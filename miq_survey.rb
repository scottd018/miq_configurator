#!/usr/bin/env ruby
#
# Author:      Dustin Scott, Red Hat
# Description: The initial file to run to generate the inventory and vars file for Ansible
# Created On:  10-Sep-2018
#

require 'open3'

def format_question_valid_data(valid_data)
  return valid_data unless valid_data

  if valid_data.kind_of?(Range)
    ['<', valid_data.first, '-', valid_data.last, '>'].join
  else
    ['<', valid_data.join(' | '), '>'].join
  end
end

def format_question_default_value(default_value)
  return default_value unless default_value

  ['[', default_value, ']'].join
end

def format_question(question, valid_data, default_value)
  [
    question,
    ':',
    format_question_valid_data(valid_data),
    format_question_default_value(default_value),
    ' '
  ].compact.join(' ')
end

def print_header(header_text)
  puts
  puts '#'
  puts "# #{header_text}"
  puts '#'
  puts
end

def ask_question(question, valid_data = nil, default_value = nil, protected = false)
  print format_question(question, valid_data, default_value)
  response = nil
  if protected
    require 'io/console'
    response = STDIN.noecho(&:gets).chomp
    puts
  else
    response = gets.chomp
  end
  response = default_value if response.empty?

  begin
    response = Integer(response)
  rescue ArgumentError
    # There is nothing to do if the response is not an integer
  end

  if valid_data
    unless valid_data.include?(response)
      puts "Invalid response: <#{response}>.  Please re-input your response..."
      response = ask_question(question, valid_data, default_value)
    end
  end

  if response.nil?
    puts "No response given.  Please re-input your response..."
    response = ask_question(question, valid_data, default_value)
  end

  response
end

def parse_extra_vars(extra_vars_hash)
  parsed_extra_vars = []
  extra_vars_hash.each do |k, v|
    parsed_extra_vars << [k, "'#{v}'"].join('=')
  end

  parsed_extra_vars.join(' ')
end

def run_ansible(extra_vars_hash, protected = false)
  parsed_extra_vars = parse_extra_vars(extra_vars_hash)
  command           = "ANSIBLE_CONFIG='./ansible_survey.cfg' ansible-playbook -vv -i 'localhost,' -c local -e \"#{parsed_extra_vars}\" miq_survey.yml"

  if protected
    puts "running ansible command: COMMAND REDACTED"
  else
    puts "running ansible command: #{command}"
  end

  begin
    _stdout, _stderr, status = Open3.capture3(command)
  rescue StandardError => e
    raise "Error running ansible command: #{e.message}"
  end

  if status != 0
    raise "ansible exited with status #{status}. Cannot continue."
  end
end

@inventory_data = {
  'all' => {
    'children' => {}
  }
}

# data that should be encrypted
# TODO: for now this is global and cannot be customized per region
print_header('COLLECTING PROTECTED VARIABLES')
protected_deployment_data = {
  'miq_group_vars_tag'          => 'protected',
  'miq_protected_vars_file'     => "#{`pwd`.chomp}/vars/miq_protected_vars.yml",
  'miq_appliance_root_password' => ask_question("What is the DESIRED password for the root user (SSH) of all appliances?", nil, nil, true),
  'miq_admin_password'          => ask_question("What is the DESIRED password for the 'admin' (application) user?",        nil, nil, true),
  'miq_vmdb_password'           => ask_question("What is the DESIRED password for the database (postgresql)?",             nil, nil, true)
}
print_header('WRITING PROTECTED VARIABLES')
run_ansible(protected_deployment_data, true)

# write and encrypt the data
system("ansible-vault encrypt --vault-id miq_configurator@prompt \'#{protected_deployment_data['miq_protected_vars_file']}\'")

print_header('COLLECTING UNIVERSAL VARIABLES')
number_of_regions = ask_question("How many regions are being deployed?", (1..999))
universal_deployment_data = {
  'miq_group_vars_tag'      => 'universal',
  'miq_universal_vars_file' => "#{`pwd`.chomp}/group_vars/all.yml",
  'miq_config_method'       => ask_question("What method should Ansible use to configure?", %w[rails],     'rails'),
  'miq_deployment_type'     => ask_question("What type of deployment is this?",             %w[appliance], 'appliance'),
  'miq_company_name'        => ask_question("What company name should be assigned?",        nil,           nil)
}

# set a global target host variable used to determine which global host to connect to for configuration
@miq_global_target_host = nil

# loop through the regions and collect data to store in the inventory data hash
1.upto(number_of_regions.to_i) do |region_index|
  print_header("COLLECTING REGIONAL VARIABLES: INDEX [#{region_index}]")

  # collect data for the playbook to setup region group vars first
  region_deployment_data = {
    'miq_group_vars_tag' => 'regional',
    'miq_region_number'  => ask_question("Region Index #{region_index}: What MIQ region number should be used for this deployment?", (1..999)),
    'miq_region_type'    => ask_question("Region Index #{region_index}: What type of region is this?", %w[global remote], 'remote'),
    'miq_vmdb_type'      => ask_question("Region Index #{region_index}: What VMDB type is being used?", %w[shared standalone], 'shared')
  }

  # get the group name so that we can be consistent between inventory file and group vars
  region_group_name = ["r#{region_deployment_data['miq_region_number']}", 'region', region_deployment_data['miq_region_type']].join('_')
  region_deployment_data['miq_regional_vars_file'] = "#{`pwd`.chomp}/group_vars/#{region_group_name}.yml"

  # collect children (zone) data
  number_of_zones = ask_question("Region Index #{region_index}: How many zones are in this region?", (1..999))
  @inventory_data['all']['children'].merge!(region_group_name.to_s => { 'children' => {} })

  # loop through the zones and collect data to store in the inventory data hash
  @miq_region_target_host = true
  1.upto(number_of_zones.to_i) do |zone_index|
    print_header("COLLECTING ZONE VARIABLES FOR REGION NUMBER [#{region_deployment_data['miq_region_number']}]: INDEX [#{zone_index}]")

    # collect data for playbook to setup zone group vars first
    zone_deployment_data = {
      'miq_group_vars_tag' => 'zone',
      'miq_standard_type'  => ask_question("---- Zone Index #{zone_index}: What type of zone is this?", %w[ui worker vmdb custom]),
      'miq_zone_name'      => ask_question("---- Zone Index #{zone_index}: What is the name of this zone?"),
      'miq_vmdb_type'      => region_deployment_data['miq_vmdb_type']
    }

    # sanitize our zone deployment data
    zone_deployment_data['miq_server_type'] = if zone_deployment_data['miq_standard_type'] == 'vmdb'
                                                'vmdb'
                                              elsif zone_deployment_data['miq_standard_type'] == 'ui' || zone_deployment_data['miq_standard_type'] == 'worker'
                                                'application'
                                              else
                                                ask_question("---- Zone Index #{zone_index}: What type of server is in this zone?", %w[vmdb application])
                                              end
    zone_deployment_data['miq_zone_display_name'] = ask_question("---- Zone Index #{zone_index}: What is the display name of this zone?", nil, zone_deployment_data['miq_zone_name'])
    region_deployment_data['miq_vmdb_disk']       = ask_question("---- Zone Index #{zone_index}: What disk should the VMDB(s) use?", nil, '/dev/sdb') if zone_deployment_data['miq_server_type'] == 'vmdb'

    # get the group name so that we can be consistent between inventory file and group vars
    zone_group_name = ["r#{region_deployment_data['miq_region_number']}", 'zone', zone_deployment_data['miq_standard_type'], zone_index].join('_')
    zone_deployment_data['miq_zone_vars_file'] = "#{`pwd`.chomp}/group_vars/#{zone_group_name}.yml"

    # collect children (host) data
    number_of_hosts = ask_question("---- Zone Index #{zone_index}: How many MIQ servers are in this zone?", (1..999))
    @inventory_data['all']['children'][region_group_name.to_s]['children'].merge!(zone_group_name.to_s => { 'hosts' => {} })

    # loop through the hosts and collect data to store in the inventory data hash
    @miq_zone_target_host = true
    1.upto(number_of_hosts.to_i) do |host_index|
      fqdn = ask_question("---- ---- Host Index #{host_index}: What is the FQDN of this host?")
      @inventory_data['all']['children'][region_group_name.to_s]['children'][zone_group_name.to_s]['hosts'].merge!(fqdn => {})

      # set the zone/region target host
      if region_deployment_data['miq_vmdb_type'] != 'standalone' || (region_deployment_data['miq_vmdb_type'] == 'standalone' && zone_deployment_data['miq_server_type'] == 'application')
        @inventory_data['all']['children'][region_group_name.to_s]['children'][zone_group_name.to_s]['hosts'][fqdn]['miq_zone_target_host']   = @miq_zone_target_host
        @inventory_data['all']['children'][region_group_name.to_s]['children'][zone_group_name.to_s]['hosts'][fqdn]['miq_region_target_host'] = @miq_region_target_host
        @miq_zone_target_host   = false
        @miq_region_target_host = false
      else
        @inventory_data['all']['children'][region_group_name.to_s]['children'][zone_group_name.to_s]['hosts'][fqdn]['miq_zone_target_host']   = false
        @inventory_data['all']['children'][region_group_name.to_s]['children'][zone_group_name.to_s]['hosts'][fqdn]['miq_region_target_host'] = false
      end

      # set the global target host
      if region_deployment_data['miq_region_type'] == 'global'
        if (zone_deployment_data['miq_server_type'] == 'application') || (region_deployment_data['miq_vmdb_type'] == 'shared' && zone_deployment_data['miq_server_type'] == 'vmdb')
          @miq_global_target_host ||= fqdn
        end
      end

      # set the target vmdb host as the first one we find
      if zone_deployment_data['miq_standard_type'] == 'vmdb'
        @miq_vmdb_target ||= fqdn
      end
    end

    # write the zone variables
    print_header("WRITING ZONE VARIABLES FOR REGION NUMBER [#{region_deployment_data['miq_region_number']}]: INDEX [#{zone_index}]")
    run_ansible(zone_deployment_data)
  end

  # add the vmdb target host for the region and reset the variable for the next region
  region_deployment_data['miq_vmdb_target'] = @miq_vmdb_target
  @miq_vmdb_target = nil

  # write the regional variables
  print_header("WRITING REGIONAL VARIABLES FOR REGION NUMBER [#{region_deployment_data['miq_region_number']}]: INDEX [#{region_index}]")
  run_ansible(region_deployment_data)
end

# write the universal variables
universal_deployment_data['miq_global_target_host'] = @miq_global_target_host
print_header('WRITING UNIVERSAL VARIABLES')
run_ansible(universal_deployment_data)

# generate the inventory
print_header("WRITING INVENTORY")
require 'yaml'
File.write('./hosts.yml', @inventory_data.to_yaml)

# display a usage message to the user
print_header("HOW TO RUN THE MIQ_CONFIGURATOR PLAYBOOK")
puts "WARNING: Before running the playbook, please review and edit the variables in: "
puts "  - group_vars/*.yml"
puts "  - vars/*.yml (vars/miq_protected_vars.yml is vault encrypted)"
puts "  - hosts.yml"
puts
puts "After variables have been reviewed, run the playbook using the following command: "
puts
puts "        ansible-playbook -vv site.yml --vault-id miq_configurator@prompt -t install,config,update"
puts
puts
