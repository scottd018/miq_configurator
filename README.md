# MIQ Configurator

## Requirements

### MIQ Requirements

- **Tested with 5.9**
- **Tested with 5.10**
- **MUST** have one VMDB zone per region
- Does not support tenants or tenanted models
- 'admin' user access to the API
- 'root' access to the operating system
- Use miq_survey.rb to generate Ansible Inventory for CloudForms

### Ansible Requirements

- **Tested with Ansible >= 2.6**
- System using Ansible must have Ruby installed to use miq_survey.rb
- If using miq_providers to add providers - pip install <git+https://github.com/ManageIQ/manageiq-api-client-python.git>
  - See <https://github.com/ManageIQ/manageiq-api-client-python/>
- Use miq_survey.rb to generate Ansible Inventory for CloudForms

### Update Requirements (Red Hat Portal or Red Hat Satellite)

- Must set variable **miq_update_registration_type** equal to one of the following:
  - 'redhat_portal'
  - 'satellite_credentials'
  - 'satellite_activation_key'
- Must have valid subscriptions available for the version of the product installed (e.g. 5.10)
- Must have the version of the Yum repository available in the content provider (e.g. cf*-5.10-for-rhel-7-rpms)
  - This means that the content view must have the repository when using Satellite

### Network Requirements

- MIQ appliances pre-provisioned and configured with an IP address on the network
- Ansible host must be able to reach MIQ appliances over port 22 (SSH)
- MIQ appliances must have internet connectivity
  - Required to clone Git Repos for miq-utilities role
  - Required to clone Git Repos for object import via miq-import

---

## Getting Started

### Cloning the Repository

Clone this repository to a server which has Ansible installed.

```bash
git clone https://gitlab.consulting.redhat.com/cloud-management-practice/miq_configurator.git
cd miq_configurator
```

---

### Generating the Files

Simply run the inventory script to generate the inventory and group vars in the proper format.  Examples still exist in the **examples** directory for more advanced deployments.

**IMPORTANT:** the survey script gives a base deployment structure.  Variables in group_vars/ and the inventory (hosts.yml) should be reviewed for completeness following the script run. These variables can also be modified to include additional customizations for each deployment, which are not a part of the survey process (e.g. providers, NTP settings, SMTP settings, etc.).  Examples of these settings are included in the files in the examples/ directory.

```bash
ruby miq_survey.rb   -OR-
./miq_survey.rb
```

The survey should look something like this:

```bash
#
# COLLECTING PROTECTED VARIABLES
#

What is the DESIRED password for the root user (SSH) of all appliances? :  redhat
What is the DESIRED password for the 'admin' (application) user? :  redhat
What is the DESIRED password for the database (postgresql)? :  redhat

#
# WRITING PROTECTED VARIABLES
#

running ansible command: ansible-playbook -vv -i 'localhost,' -e "miq_group_vars_tag='protected' miq_protected_vars_file='/Users/dscott/RubymineProjects/rhc-gitlab/miq_configurator/vars/miq_protected_vars.yml' ansible_ssh_pass='smartvm' miq_appliance_root_password='redhat' miq_admin_password='redhat' miq_vmdb_password='redhat'" miq_survey.yml
New Vault password (miq_configurator): 12345
Confirm New Vault password (miq_configurator): 12345

Encryption successful

#
# COLLECTING UNIVERSAL VARIABLES
#

How many regions are being deployed? : <1-999>  1
What method should Ansible use to configure? : <rails> [rails]
What type of deployment is this? : <appliance> [appliance]
What company name should be assigned? :  Example Company 5

#
# COLLECTING REGIONAL VARIABLES: INDEX [1]
#

Region Index 1: What MIQ region number should be used for this deployment? : <1-999>  1
Region Index 1: What type of region is this? : <global | remote> [remote]
Region Index 1: What VMDB type is being used? : <shared | standalone> [shared]
Region Index 1: How many zones are in this region? : <1-999>  3

#
# COLLECTING ZONE VARIABLES FOR REGION NUMBER [1]: INDEX [1]
#

---- Zone Index 1: What type of zone is this? : <ui | worker | vmdb | custom>  vmdb
---- Zone Index 1: What is the name of this zone? :  vmdb_zone
---- Zone Index 1: What is the display name of this zone? : [vmdb_zone]
---- Zone Index 1: What disk should the VMDB(s) use? : [/dev/sdb]
---- Zone Index 1: How many MIQ servers are in this zone? : <1-999>  1
---- ---- Host Index 1: What is the FQDN of this host? :  vmdb.example.com

#
# WRITING ZONE VARIABLES FOR REGION NUMBER [1]: INDEX [1]
#

running ansible command: ansible-playbook -vv -i 'localhost,' -e "miq_group_vars_tag='zone' miq_standard_type='vmdb' miq_zone_name='vmdb_zone' miq_zone_display_name='vmdb_zone' miq_server_type='vmdb' miq_vmdb_disk='/dev/sdb' miq_zone_vars_file='/Users/dscott/RubymineProjects/miq_configurator/group_vars/1_zone_vmdb_1.yml'" miq_survey.yml

#
# COLLECTING ZONE VARIABLES FOR REGION NUMBER [1]: INDEX [2]
#

---- Zone Index 2: What type of zone is this? : <ui | worker | vmdb | custom>  ui
---- Zone Index 2: What is the name of this zone? :  ui_zone
---- Zone Index 2: What is the display name of this zone? : [ui_zone]
---- Zone Index 2: How many MIQ servers are in this zone? : <1-999>  1
---- ---- Host Index 1: What is the FQDN of this host? :  ui.example.com

#
# WRITING ZONE VARIABLES FOR REGION NUMBER [1]: INDEX [2]
#

running ansible command: ansible-playbook -vv -i 'localhost,' -e "miq_group_vars_tag='zone' miq_standard_type='ui' miq_zone_name='ui_zone' miq_zone_display_name='ui_zone' miq_server_type='application' miq_zone_vars_file='/Users/dscott/RubymineProjects/miq_configurator/group_vars/1_zone_ui_2.yml'" miq_survey.yml

#
# COLLECTING ZONE VARIABLES FOR REGION NUMBER [1]: INDEX [3]
#

---- Zone Index 3: What type of zone is this? : <ui | worker | vmdb | custom>  worker
---- Zone Index 3: What is the name of this zone? :  worker_zone
---- Zone Index 3: What is the display name of this zone? : [worker_zone]
---- Zone Index 3: How many MIQ servers are in this zone? : <1-999>  1
---- ---- Host Index 1: What is the FQDN of this host? :  worker.example.com

#
# WRITING ZONE VARIABLES FOR REGION NUMBER [1]: INDEX [3]
#

running ansible command: ansible-playbook -vv -i 'localhost,' -e "miq_group_vars_tag='zone' miq_standard_type='worker' miq_zone_name='worker_zone' miq_zone_display_name='worker_zone' miq_server_type='application' miq_zone_vars_file='/Users/dscott/RubymineProjects/miq_configurator/group_vars/1_zone_worker_3.yml'" miq_survey.yml

#
# WRITING REGIONAL VARIABLES: INDEX [1]
#

running ansible command: ansible-playbook -vv -i 'localhost,' -e "miq_group_vars_tag='regional' miq_region_number='1' miq_region_type='remote' miq_vmdb_type='shared' miq_regional_vars_file='/Users/dscott/RubymineProjects/miq_configurator/group_vars/1_region_remote.yml' miq_vmdb_target='vmdb.example.com'" miq_survey.yml

#
# WRITING UNIVERSAL VARIABLES
#

running ansible command: ansible-playbook -vv -i 'localhost,' -e "miq_group_vars_tag='universal' miq_universal_vars_file='/Users/dscott/RubymineProjects/miq_configurator/group_vars/all.yml' miq_config_method='rails' miq_deployment_type='appliance' miq_company_name='Example Company 5' miq_admin_password='smartvm5' miq_vmdb_password='smartvm5' miq_appliance_root_password='redhat5' miq_global_target_host=''" miq_survey.yml

#
# WRITING INVENTORY
#


#
# HOW TO RUN THE MIQ_CONFIGURATOR PLAYBOOK
#

WARNING: Before running the playbook, please review and edit the variables in:
  - group_vars/*.yml
  - vars/*.yml (vars/miq_protected_vars.yml is vault encrypted)
  - hosts.yml

After variables have been reviewed, run the playbook using the following command:

        ansible-playbook -vv site.yml --vault-id miq_configurator@prompt -t install,config,update

```

---

### Configurable Variables

#### Inventory

The inventory contains a nested listing of MIQ hosts which are to be managed by this tool.  The structure contains
several different levels (Global > Region > Zone > Host).  The function of the playbook depends on this structure
and therefore must not be manipulated.

The below outlines how each level of nesting is applied within the inventory file.  It should be noted that each
layer of nesting has an associated variables file.  This file is located in the **group_vars** directory with the
name of the group matching a YAML file name of the variables file (e.g. host_group = region_1, vars_file =
group_vars/region_1.yml).  All variables should be kept there in order to keep the inventory small and limited to
host information (host vars) only.

```yaml
#
# GLOBAL DEPLOYMENT INFO
#
all:
  children:
    #
    # REGION DEPLOYMENT INFO
    #
    1_region_remote:
      children:
        #
        # ZONE DEPLOYMENT INFO
        #
        1_zone_vmdb_1:
          hosts:
          #
          # HOST DEPLOYMENT INFO
          # NOTE: HOSTS BELONG IN ZONE HOST GROUPS
          #
            'cfmevmdb001.omaha.mycompany.com':
              miq_zone_target_host:   false
              miq_region_target_host: false
        1_zone_worker_2:
          hosts:
            'cfmewrk001.omaha.mycompany.com':
              miq_zone_target_host:   true
              miq_region_target_host: true
              miq_primary_server_roles:
                - ems_inventory
            'cfmewrk002.omaha.mycompany.com':
              miq_zone_target_host:   false
              miq_region_target_host: false
              miq_primary_server_roles:
                - event
                - ems_metrics_coordinator
        1_zone_ui_3:
          hosts:
            'cfmeui001.omaha.mycompany.com':
              miq_zone_target_host:   true
              miq_region_target_host: false
              miq_primary_server_roles:
                - notifier
                - git_owner
            'cfmeui002.omaha.mycompany.com':
              miq_zone_target_host:   false
              miq_region_target_host: false
              miq_primary_server_roles:
                - scheduler
```

#### Global (group_vars/all.yml)

This file contains all of the global configuration variables.  Variables which are set here are applied to all MIQ
servers which are maintained in the inventory.  This may be ideal when you want to share a v2_key across multiple
regions or share common configuration settings across multiple regions (e.g. LDAP servers, NTP servers, SMTP servers, etc.)

#### Region (group_vars/<REGION_NUM>_region_<REGION_TYPE>.yml)

This file contains all of the configuration variables which are specific to a MIQ region.

#### Zone (group_vars/<REGION_NUM>_zone_<ZONE_TYPE>_<ZONE_INDEX>_.yml)

This file contains all of the configuration variables which are specific to a MIQ zone.  Examples of specific zone
settings might be role settings and provider assignments.

#### Host (hosts.yml)

The inventory contains all variables which are directly related to hosts.  Host variables can also be split out into
 a **host_vars/inventory_hostname.yml** structure, but they were kept in the inventory for simplicity.

#### Ansible Variable Inheritance

This is simply a recommended structure and can be manipulated based on the skillset of the individual working with
this tool.  For more information on Ansible variable inheritance, please see
<http://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable.>

#### Complete Variable List

The below is a complete list of variables, along with their recommended assignment level (global, region, zone, or
host), and proper data types.  Samples and formats of these variables are in the **examples/*** files.

| variable                            | required (install/config)         | applies to global/region/zone/host       | type          | default                  | description |
| ------------------------------------| --------------------------------- | ---------------------------------------- | ------------- | ------------------------ | ----------- |
| debug                               | no  (install/config)              | global/region/zone/host                  | boolean       | false                    | Enable debug logging |
| miq_appliance_root_password         | yes (config)                      | **global**/region/zone/host              | string        |                          | Update appliance root password.  Define in miq_protected.yml vault. |
| miq_region_number                   | yes (install)                     | **region**                               | integer       |                          | Region number to install, between 1-999 (must be unique) |
| miq_vmdb_disk                       | yes (install)                     | **region**                               | string        |                          | Disk to initialize for the VMDB during installation |
| miq_vmdb_type                       | yes (install/config)              | **region**                               | string        | 'shared'                 | A 'shared' (runs the MIQ app) or 'standalone' (runs PostgreSQL only) VMDB |
| miq_vmdb_target                     | yes (install)                     | **region**                               | string        |                          | Target VMDB which generated the V2 key to be used for an SCP pull onto other appliances during installation |
| miq_vmdb_password                   | no (install)                      | **global**/region                        | string        | 'smartvm'                | Password to the VMDB set during installation |
| miq_region_type                     | no (config)                       | **region**                               | string        |                          | Type of region being deployed (global vs. remote) |
| miq_deployment_type                 | yes (install/config)              | **global**/region                        | string        | 'appliance'              | Types of MIQ appliances being installed/configured. Must be either 'appliance' OR 'container' |
| miq_config_method                   | yes (install/config)              | **global**                               | string        | 'rails'                  | Integration type to use for configuration: 'rest' or 'rails' are only valid |
| miq_admin_username                  | yes (config)                      | **global**/region                        | string        | 'admin'                  | Username for the 'admin' user |
| miq_admin_password                  | no (config)                       | **global**/region                        | string        |                          | Password for the 'admin' user |
| miq_os_timezone                     | no (config)                       | global/**region**/zone/host              | string        | 'UTC'                    | Timezone to set on the operating system as determined by `timedatectl list-timezones` |
| miq_app_timezone                    | no (config)                       | global/**region**/zone/host              | string        | 'UTC'                    | Timezone to set for the MIQ application |
| miq_company_name                    | yes (config)                      | **global**/region/zone/host              | string        |                          | Company Name to use |
| miq_smtp_settings                   | no (config)                       | global/**region**/zone/host              | dict          |                          | SMTP Settings dict |
| miq_enable_quickstart               | no (config)                       | **global**/region                        | boolean       | false                    | Common upstream automate domains and tools which may be imported |
| miq_automate_git_repos              | no (config)                       | **global**/region                        | dict          |                          | Ordered list of dicts of automate domains which need to be pulled from git (top = lowest priority, bottom = highest priority) |
| miq_objects_repo                    | no (config)                       | **global**/region                        | dict          |                          | Repository containing MIQ objects based on the miqimport/miqexport utility |
| miq_objects                         | no (config)                       | **global**/region                        | list          |                          | List of objects to import from miq_objects_repo |
| miq_groups                          | no (config)                       | **global**/region                        | list of dicts |                          | Dictionary of group settings with a role assignment - requires role import from miq_objects/miq_objects_repo |
| miq_v2_key_content                  | no (install)                      | **global**/region                        | block         |                          | v2_key to use (used only for previous installations).  One is generated (default) if not specified |
| miq_load_balanced                   | no (config)                       | **zone**                                 | boolean       | false                    | Configure appliances for load-balancing |
| miq_zone_name                       | yes (config)                      | **zone**                                 | string        |                          | Name of the zone to create in the manageiq application (configured once per zone) |
| miq_zone_display_name               | yes (config)                      | **zone**                                 | string        |                          | Display name of the zone as it appears in the user interface (configured once per zone) |
| miq_server_type                     | yes (install/config)              | global/region/**zone**/host              | string        |                          | Type of server as identified by 'application' or 'vmdb' |
| miq_ntp_servers                     | no (config)                       | global/**region**/zone/host              | list          |                          | List of ntp server ip addresses or hostnames |
| miq_providers                       | no (config)                       | **zone**                                 | list of dicts |                          | List of dictionary items which represent miq_provider objects from the manageiq_provider module (see <https://docs.ansible.com/ansible/latest/modules/manageiq_provider_module.html)> |
| miq_ansible_mem_limit               | no (config)                       | global/region/**zone**/host              | string        |                          | Embedded Ansible memory threshold (e.g. '500.megabytes) |
| miq_connection_broker_mem_limit     | no (config)                       | global/region/**zone**/host              | string        |                          | Connection Broker memory threshold (e.g. '500.megabytes) |
| miq_cu_data_collector_mem_limit     | no (config)                       | global/region/**zone**/host              | string        |                          | Capacity and Utilization Data Collector memory threshold (e.g. '500.megabytes) |
| miq_cu_data_collector_count         | no (config)                       | global/region/**zone**/host              | integer       |                          | Capacity and Utilization Data Collector worker thread count |
| miq_cu_data_processor_mem_limit     | no (config)                       | global/region/**zone**/host              | string        |                          | Capacity and Utilization Data Processor memory threshold (e.g. '500.megabytes) |
| miq_cu_data_processor_count         | no (config)                       | global/region/**zone**/host              | integer       |                          | Capacity and Utilization Data Processor worker thread count |
| miq_default_worker_mem_limit        | no (config)                       | global/region/**zone**/host              | string        |                          | Default Worker memory threshold (e.g. '500.megabytes) |
| miq_default_worker_count            | no (config)                       | global/region/**zone**/host              | integer       |                          | Default Worker thread count |
| miq_event_monitor_mem_limit         | no (config)                       | global/region/**zone**/host              | string        |                          | Event Monitor memory threshold (e.g. '500.megabytes) |
| miq_event_handler_mem_limit         | no (config)                       | global/region/**zone**/host              | string        |                          | Event Handler memory threshold (e.g. '500.megabytes) |
| miq_generic_worker_mem_limit        | no (config)                       | global/region/**zone**/host              | string        | '900.megabytes'          | Generic Worker memory threshold (e.g. '500.megabytes) |
| miq_generic_worker_count            | no (config)                       | global/region/**zone**/host              | integer       |                          | Generic Worker thread count |
| miq_priority_worker_mem_limit       | no (config)                       | global/region/**zone**/host              | string        | '900.megabytes'          | Priority Worker memory threshold (e.g. '500.megabytes) |
| miq_priority_worker_count           | no (config)                       | global/region/**zone**/host              | integer       |                          | Priority Worker thread count |
| miq_refresh_worker_mem_limit        | no (config)                       | global/region/**zone**/host              | string        |                          | EMS Refresh Worker memory threshold (e.g. '500.megabytes) |
| miq_reporting_worker_mem_limit      | no (config)                       | global/region/**zone**/host              | string        |                          | Reporting Worker memory threshold (e.g. '500.megabytes) |
| miq_reporting_worker_count          | no (config)                       | global/region/**zone**/host              | integer       |                          | Reporting Worker thread count |
| miq_schedule_worker_mem_limit       | no (config)                       | global/region/**zone**/host              | string        |                          | Schedule Worker memory threshold (e.g. '500.megabytes) |
| miq_ui_worker_mem_limit             | no (config)                       | global/region/**zone**/host              | string        |                          | UI Worker memory threshold (e.g. '500.megabytes) |
| miq_ui_worker_count                 | no (config)                       | global/region/**zone**/host              | integer       |                          | UI Worker thread count |
| miq_vm_analysis_collector_mem_limit | no (config)                       | global/region/**zone**/host              | string        |                          | Smart State Analysis Worker memory threshold (e.g. '500.megabytes) |
| miq_vm_analysis_collector_count     | no (config)                       | global/region/**zone**/host              | integer       |                          | Smart State Analysis Worker thread count |
| miq_web_service_worker_mem_limit    | no (config)                       | global/region/**zone**/host              | string        |                          | Web Service Worker memory threshold (e.g. '500.megabytes) |
| miq_web_service_worker_count        | no (config)                       | global/region/**zone**/host              | integer       |                          | Web Service Worker thread count |
| miq_web_socket_worker_mem_limit     | no (config)                       | global/region/**zone**/host              | string        |                          | Web Socket Worker memory threshold (e.g. '500.megabytes) |
| miq_web_socket_worker_count         | no (config)                       | global/region/**zone**/host              | integer       |                          | Web Socket Worker thread count |
| miq_server_roles                    | no (config)                       | **zone**/host                            | list          |                          | Server roles to enable |
| miq_primary_server_roles            | no (config)                       | **host**                                 | list          |                          | List of roles which should be promoted to primary/preferred |
| miq_tertiary_server_roles           | no (config)                       | **host**                                 | list          |                          | NOT COMMON: List of roles which should be promoted to tertiary |
| miq_cu_collection_clusters_enabled  | no (config)                       | global/**region**                        | boolean       | true                     | Enable C&U collection for all hosts and clusters |
| miq_cu_collection_storages_enabled  | no (config)                       | global/**region**                        | boolean       | true                     | Enable C&U collection for all datastores |
| miq_zone_target_host                | yes (config)                      | **host**                                 | boolean       | false                    | A host to target for run_once tasks for each zone.  Requires 1 miq_zone_target_host per zone. |
| miq_region_target_host              | yes (config)                      | **host**                                 | boolean       | false                    | A host to target for run_once tasks for each region.  Requires 1 miq_region_target_host per region. |
| miq_global_target_host              | no (config)                       | **global**                               | string        |                          | A host to target in a global region to configure replication settings.  Required for global deployments. |
| miq_os_hostname                     | no (config)                       | **host**                                 | string        | inventory_hostname       | Hostname to set on the operating system |
| miq_app_hostname                    | no (config)                       | **host**                                 | string        | inventory_hostname_short | Hostname to set as the MIQ server name in the user interface |
| miq_external_auth_type              | no (config)                       | global/**region**/zone/host              | string        |                          | Type of external authentication to setup (only active_directory supported right now) |
| miq_ldap_uri                        | no (config)                       | global/**region**/zone/host              | string        |                          | URI to the LDAP server, to include the port number |
| miq_ldap_base_dn                    | no (config)                       | global/**region**/zone/host              | string        |                          | Base DN of the LDAP search tree |
| miq_ldap_user_search_base           | no (config)                       | global/**region**/zone/host              | string        |                          | DN of the user search tree |
| miq_ldap_group_search_base          | no (config)                       | global/**region**/zone/host              | string        |                          | DN of the group search tree |
| miq_ldap_bind_dn                    | no (config)                       | global/**region**/zone/host              | string        |                          | DN of the user to bind to LDAP with |
| miq_ldap_bind_password              | no (config)                       | global/**region**/zone/host              | string        |                          | Password of the user who is binding to LDAP |
| miq_ldap_domain                     | no (config)                       | global/**region**/zone/host              | string        |                          | LDAP Domain to use |
| miq_update_registration_type        | no (config)                       | **global**/region                        | string        |                          | Type of registration for updates ('satellite_credentials', 'satellite_activation_key' or 'redhat_portal' are valid). |
| satellite_server                    | no (config)                       | **global**/region                        | string        |                          | Satellite Server to register with (valid for 'satellite_credentials' or 'satellite_activaiton_key' miq_update_registration_type). |
| satellite_activation_key            | no (config)                       | **global**/region                        | string        |                          | Activation key to use to register to Satellite with (valid for 'satellite_activation_key' miq_update_registration_type). |
| satellite_organization              | no (config)                       | **global**/region                        | string        |                          | Satellite organization to register with (valid only for 'satellite_credentials' or 'satellite_activaiton_key' miq_update_registration_type). |
| satellite_environment               | no (config)                       | **global**/region                        | string        | 'Library'                | Satellite lifecycle environment to register into (valid only for 'satellite_credentials' miq_update_registration_type). |
| satellite_username                  | no (config)                       | **global**/region                        | string        |                          | Satellite username to register with (valid only for 'satellite_credentials' miq_update_registration_type). |
| satellite_password                  | no (config)                       | **global**/region                        | string        |                          | Satellite password to register with (valid only for 'satellite_credentials' miq_update_registration_type). |
| satellite_pool_ids                  | no (config)                       | **global**/region                        | list          |                          | GUID of the CloudForms subscription pool IDs to attach to the systems.  Defaults to auto-attach if none specified (valid only for 'satellite_credentials' miq_update_registration_type). |
| redhat_portal_username              | no (config)                       | **global**/region                        | string        |                          | Red Hat portal username to register with (valid only for 'redhat_portal' miq_update_registration_type). |
| redhat_portal_password              | no (config)                       | **global**/region                        | string        |                          | Red Hat portal password to register with (valid only for 'redhat_portal' miq_update_registration_type). |
| redhat_portal_pool_ids              | no (config)                       | **global**/region                        | list          |                          | GUID of the CloudForms subscription pool IDs to attach to the systems.  Defaults to auto-attach if none specified (valid only for 'redhat_portal' miq_update_registration_type). |

**NOTE:** bolded items above indicated recommended targets for settings

#### Using Encrypted Variables

**NOTE:** when using the survey to generate the inventory and group vars files (see [Generating the Files](#generating-the-files)), this step is automatically performed as part of the survey process.

During the exercise of configuring variables and inventory, some things such as application password and database
passwords are required for install and configuration.  It is likely desirable that these items are not to
remain in cleartext, as they are considered to be sensitive.  To encrypt, all variables should be stored in the
**vars/miq_protected_vars.yml** file.

Here is an example of how to store variables, encrypt them, and use them in the playbook:

1. Encrypt the **vars/miq_protected_vars.yml** file with your own password.
Be sure that your variable names match those defined in [Complete Variable List](#complete-variable-list):

    ```bash
    ansible-vault encrypt --vault-id miq_configurator@prompt vars/miq_protected_vars.yml
    New Vault password (miq_configurator):
    Confirm New Vault password (miq_configurator):
    Encryption successful
    ```

2. Edit the encrypted file with `ansible-vault` and write your encrypted variables to the **vars/miq_protected_vars.yml**
file.  Be sure to save using standard VI commands when you are finished:

    ```bash
    ansible-vault edit --vault-id miq_configurator@prompt vars/miq_protected_vars.yml
    Vault password:
    miq_admin_password: MyP@ssw0rd
    miq_vmdb_password:  MyP@ssw0rd
    ```

3. The file will now be encrypted and will look something like this:

    ```bash
    cat vars/miq_protected_vars.yml
    $ANSIBLE_VAULT;1.1;AES256
    39353163643931363839383135663833313235383237323430353536303239353965626564613432
    3065326534323336376335376164633831356437343639620a623666383532353865323262323539
    62393634663337303639373065333333626461346132373262353539316433646164353431343765
    3239623337613765650a356234656237316662363836663138623861613062366662366530353637
    3563
    ```

4. The file is automatically referenced from the main playbook, so no additional work is needed to pull in those new variables:

    ```bash
      vars_files:
        - vars/miq_custom_vars.yml
        - vars/miq_vars.yml
        - vars/miq_protected_vars.yml
    ```

5. When using the `ansible-playbook` command to run the playbook, be sure to use the `--vault-id miq_configurator@prompt` flag so that
Ansible will decrypt the file before running the playbook.  See [Running the Installer](#running-the-installer) for
more information on using the `ansible-playbook` binary with the `--vault-id miq_configurator@prompt` flag.

#### Using Custom Variables

During the exercise of configuring variables and inventory, it may be desirable to use your own custom variable, to
avoid duplication of content.  An example of an item which may be duplicated is using a domain name variable, and
using that domain name in things like SMTP settings, NTP settings,
and LDAP settings to point to their specific servers.

1. Write your custom variables to the **vars/miq_custom_vars.yml** file:

    ```yaml
    miq_domain_name: example.com
    ```

2. Use your custom variables in any of the **group_vars/\*.yml** files with Jinja2 style templating:

    ```yaml
    miq_smtp_settings:
      authentication:       'none'
      domain:               "{{ miq_domain_name }}"
      from:                 "svc.cloud@{{ miq_domain_name }}"
      host:                 "mail.{{ miq_domain_name }}"
      port:                 25
      username:             'svc.cloud'
      enable_starttls_auto: false
      openssl_verify_mode:  'none'
    ```

#### Ansible Tags

This playbook requires the use of Ansible tags to control what pieces of the playbook run.  All tags will use the
`miq_region_host_group` input to perform specific parts of the playbook against that region, or will use all hosts from the `hosts.yml` file.

See usage at [Running the Installer](#running-the-installer).

Tags are used with the `-t` flag.

The below tags are supported:

- **install** - this tag tells the Ansible playbook to perform the installation of all hosts in the `hosts.yml` inventory, or against a single region against hosts
from the `miq_region_host_group` input.
- **config** - this tag tells the Ansible playbook to perform the configuration of all hosts in the `hosts.yml` inventory, or against a single region against hosts
from the `miq_region_host_group` input.
- **update** - this tag tells the Ansible playbook to perform an update and reboot of all hosts in the `hosts.yml` inventory, or against a single region against hosts
from the `miq_region_host_group` input.  **WARNING:** by default the update step is performed during `config`.  In order to bypass an update and reboot, you must use `--skip-tags update`.

#### Running the Installer

During the execution of miq_survey.rb, an encrypted file is created to store sensitive variables such as passwords.  When this happens, miq_survey.rb asks for a password used to encrypt the sensitive variables.  In the below example `12345` is the password used for the vault:

```bash
#
# WRITING PROTECTED VARIABLES
#

running ansible command: ansible-playbook -vv -i 'localhost,' -e "miq_group_vars_tag='protected' miq_protected_vars_file='/Users/dscott/RubymineProjects/rhc-gitlab/miq_configurator/vars/miq_protected_vars.yml' ansible_ssh_pass='redhat' miq_appliance_root_password='redhat' miq_admin_password='smartvm' miq_vmdb_password='smartvm'" miq_survey.yml
New Vault password: 12345
Confirm New Vault password: 12345
Encryption successful
```

The password used during vault encryption step of miq_survey.rb (`12345` using the example above) should be used when prompted for a password during execution of the parent/main playbook.  This is done to avoid storing the vault password in cleartext:

Before running the playbook, determine which region to run the playbook against (default is to run against ALL).  The region host group can be determined from the hosts.yml file in the format N_region_REGIONTYPE.yml.  1_region_remote is the region host group in the below example:

```yaml
#
# GLOBAL DEPLOYMENT INFO
#
all:
  children:
    1_region_remote:
```

The following is an example of executing the miq_configurator playbook with a specific region host group:

```bash
ansible-playbook -vv -e 'miq_region_host_group=1_region_remote' site.yml --vault-id miq_configurator@prompt -t install,config,update
SSH password: <ROOT_PASSWORD_HERE>
Vault password: <PASSWORD_FROM_SURVEY_HERE>
```

If multiple regions are desired, the following can be used:

```bash
ansible-playbook -vv -e 'miq_region_host_group=1_region_remote:99_region_global' site.yml --vault-id miq_configurator@prompt -t install,config,update
SSH password: <ROOT_PASSWORD_HERE>
Vault password: <PASSWORD_FROM_SURVEY_HERE>
```

If you want to run against all regions configured in the hosts.yml file, the following can be used:

```bash
ansible-playbook -vv site.yml --vault-id miq_configurator@prompt -t install,config,update
SSH password: <ROOT_PASSWORD_HERE>
Vault password: <PASSWORD_FROM_SURVEY_HERE>
```

An example of skipping the update and reboot step:

```bash
ansible-playbook -vv site.yml --vault-id miq_configurator@prompt -t install,config --skip-tags update
SSH password: <ROOT_PASSWORD_HERE>
Vault password: <PASSWORD_FROM_SURVEY_HERE>
```

## Additional Documentation

**Additional documentation can be found in the root directory for each role.**
