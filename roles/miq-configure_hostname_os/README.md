# Description
This role will change the hostname on the operating system for all appliances.  If no name is specified, the inventory hostname is used.

# Dependencies
There are no additional dependencies.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_os_hostname                     | yes      | string        | yes                               | Hostname to set on the operating system |
