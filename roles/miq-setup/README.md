# Description
This role will change the timezone in the user interface.

# Dependencies
This role has no external dependencies.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_v2_key_content                  | yes      | block         | no                                | v2_key to use (used only for previous installations).  One is generated (default) if not specified |
