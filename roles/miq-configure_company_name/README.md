# Description
This role will change the company name for all appliances.

# UI Depiction
The following shows an image of the adjustments which are being changed, from the UI perspective.

![company_name](./miq_company_name_ui.png "Company Name")

# Advanced Settings Depiction
Additionally, the advanced settings are updated with this role.  Below is a sample snippet showing the settings that are adjusted:

```
:server:
  :company: Demo Company
```

# Dependencies
This role depends on the `miq-configure_settings` role.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_company_name                    | yes      | string        | no                                | Company Name to use |
