# Description
This role will change the 'admin' user password.

# Dependencies
This role has no dependencies.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| enable_no_log                       | no       | boolean       | yes                               | Turn off logging for security reasons (e.g. passwords) |
| miq_admin_password                  | yes      | string        | no                                | Password for the 'admin' user |
| miq_admin_username                  | yes      | string        | yes                               | Username for the 'admin' user |
