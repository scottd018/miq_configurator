# Description
This role will generate the group_vars structure needed to run the miq_configurator site.yml file.

# Dependencies
This should only be run from miq_survey.rb.
