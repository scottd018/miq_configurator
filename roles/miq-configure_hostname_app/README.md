# Description
This role will change the server name for all appliances.  If no name is specified, the short inventory hostname is used.

# UI Depiction
The following shows an image of the adjustments which are being changed, from the UI perspective.

![server_name](./miq_app_hostname_ui.png "Server Name")

# Advanced Settings Depiction
Additionally, the advanced settings are updated with this role.  Below is a sample snippet showing the settings that are adjusted:

```
:server:
  :name: miq-appliance
```

# Dependencies
This role depends on the `miq-configure_settings` role.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_app_hostname                    | yes      | string        | yes                               | Hostname to set as the MIQ server name in the user interface |
