# Description
This role will configure the NTP servers at the zone level.

# UI Depiction
The following shows an image of the adjustments which are being changed, from the UI perspective.

![ntp](./miq_ntp_ui.png "NTP Settings")

# Advanced Settings Depiction
Additionally, the advanced settings are updated with this role.  Below is a sample snippet showing the settings that are adjusted:

```
:ntp:
  :server:
  - 10.10.10.10
  - 10.10.10.11
  - 10.10.10.12
```

# Dependencies
This role has no dependencies.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_ntp_servers                     | yes      | list          | no                                | List of NTP server ip addresses or hostnames |
| miq_zone_name                       | yes      | string        | no                                | Name of the zone to which the server belongs |
