# Description
This role installs and enables the VMware WebMKS on all appliances which are running the `evmserverd` process.  The WebMKS has shown to be the most reliable form of a web console for managing VMware environments, so we are opinionated about using it by default.

# UI Depiction
The following shows an image of the adjustments which are being changed, from the UI perspective.

![webmks](./miq_webmks_ui.png "WebMKS")


# Advanced Settings Depiction
Additionally, the advanced settings are updated with this role.  Below is a sample snippet showing the settings that are adjusted:

```
:server:
  :remote_console_type: 'WebMKS'
```

# Dependencies
This role depends on the `miq-configure_settings` role.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |

# Additional Information
Additional information can be found at https://access.redhat.com/solutions/3376081.
