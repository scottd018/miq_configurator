# Description
This role will enable and disable the specified roles.  Additionally, it will set roles to primary and tertiary where applicable (roles are considered 'secondary' or 'normal' by default).

# UI Depiction
The following shows an image of the adjustments which are being changed, from the UI perspective.

## Enable and Disable Roles
![server_roles](./miq_server_roles_ui.png "Server Roles")

## Prioritize Roles
![server_roles_prioritization](./miq_server_roles_prioritization_ui.png "Server Roles Prioritization")

# Dependencies
This role has no dependencies.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_server_roles                    | yes      | list          | no                                | Server roles to enable |
| miq_primary_server_roles            | no       | list          | no                                | List of roles which should be promoted to primary/preferred |
| miq_tertiary_server_roles           | no       | list          | no                                | NOT COMMON: List of roles which should be promoted to tertiary |

**NOTE**: Roles are secondary be default and do not need to be specified as such.