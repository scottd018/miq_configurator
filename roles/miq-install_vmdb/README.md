# Description
Installs the VMDB server, which are defined as those servers which run the `postgresql` service.

# Dependencies
This role should run prior to the `miq-install_application` and depends upon `miq-wait_for_application` role (only for 'shared' installations).  Application servers need to have a previously configured VMDB prior to installation, which is why this role should be run first.  For shared VMDB installations (installations which have a VMDB that run `evmserverd` and `postgresql`), after installation, we must wait for the application to become online before continuing.

Additionally, the `miq-setup` role is run when we have a pre-defined v2_key.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_region_number                   | yes      | integer       | no                                | Region number to install, between 1-999 (must be unique) |
| miq_vmdb_type                       | yes      | string        | no                                | A 'shared' (runs the MIQ app) or 'standalone' (runs PostgreSQL only) VMDB |
| miq_vmdb_target                     | yes      | string        | no                                | Target VMDB which generated the V2 key to be used for an SCP pull onto other appliances during installation |
| miq_vmdb_password                   | no       | string        | no                                | Password to the VMDB set during installation |
| miq_v2_key_content                  | no       | block         | yes                               | v2_key to use (used only for previous installations).  One is generated (default) if not specified |
| miq_vmdb_disk                       | yes      | string        | no                                | Disk to initialize for the VMDB during installation |
