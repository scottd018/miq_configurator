# Description
This role will configure the worker tuning adjustments.

# UI Depiction
The following shows an image of the adjustments which are being tuned, from the UI perspective.

![workers](./miq_workers_ui.png "Workers")

# Advanced Settings Depiction
Additionally, the advanced settings are updated with this role.  Below is a sample snippet showing the settings that are adjusted:

```
    :queue_worker_base:
      :ems_metrics_collector_worker:
        :defaults:
          :count: '4'
          :memory_threshold: 400.megabytes
```

# Dependencies
This role depends on the `miq-configure_settings` role.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_ansible_mem_limit               | no       | string        | no                                | Embedded Ansible memory threshold (e.g. '500.megabytes) |
| miq_connection_broker_mem_limit     | no       | string        | no                                | Connection Broker memory threshold (e.g. '500.megabytes) |
| miq_cu_data_collector_mem_limit     | no       | string        | no                                | Capacity and Utilization Data Collector memory threshold (e.g. '500.megabytes) |
| miq_cu_data_collector_count         | no       | integer       | no                                | Capacity and Utilization Data Collector worker thread count |
| miq_cu_data_processor_mem_limit     | no       | string        | no                                | Capacity and Utilization Data Processor memory threshold (e.g. '500.megabytes) |
| miq_cu_data_processor_count         | no       | integer       | no                                | Capacity and Utilization Data Processor worker thread count |
| miq_default_worker_mem_limit        | no       | string        | no                                | Default Worker memory threshold (e.g. '500.megabytes) |
| miq_default_worker_count            | no       | integer       | no                                | Default Worker thread count |
| miq_event_monitor_mem_limit         | no       | string        | no                                | Event Monitor memory threshold (e.g. '500.megabytes) |
| miq_event_handler_mem_limit         | no       | string        | no                                | Event Handler memory threshold (e.g. '500.megabytes) |
| miq_generic_worker_mem_limit        | no       | string        | yes                               | Generic Worker memory threshold (e.g. '500.megabytes) |
| miq_generic_worker_count            | no       | integer       | no                                | Generic Worker thread count |
| miq_priority_worker_mem_limit       | no       | string        | yes                               | Priority Worker memory threshold (e.g. '500.megabytes) |
| miq_priority_worker_count           | no       | integer       | no                                | Priority Worker thread count |
| miq_refresh_worker_mem_limit        | no       | string        | no                                | EMS Refresh Worker memory threshold (e.g. '500.megabytes) |
| miq_reporting_worker_mem_limit      | no       | string        | no                                | Reporting Worker memory threshold (e.g. '500.megabytes) |
| miq_reporting_worker_count          | no       | integer       | no                                | Reporting Worker thread count |
| miq_schedule_worker_mem_limit       | no       | string        | no                                | Schedule Worker memory threshold (e.g. '500.megabytes) |
| miq_ui_worker_mem_limit             | no       | string        | no                                | UI Worker memory threshold (e.g. '500.megabytes) |
| miq_ui_worker_count                 | no       | integer       | no                                | UI Worker thread count |
| miq_vm_analysis_collector_mem_limit | no       | string        | no                                | Smart State Analysis Worker memory threshold (e.g. '500.megabytes) |
| miq_vm_analysis_collector_count     | no       | integer       | no                                | Smart State Analysis Worker thread count |
| miq_web_service_worker_mem_limit    | no       | string        | no                                | Web Service Worker memory threshold (e.g. '500.megabytes) |
| miq_web_service_worker_count        | no       | integer       | no                                | Web Service Worker thread count |
| miq_web_socket_worker_mem_limit     | no       | string        | no                                | Web Socket Worker memory threshold (e.g. '500.megabytes) |
| miq_web_socket_worker_count         | no       | integer       | no                                | Web Socket Worker thread count |
