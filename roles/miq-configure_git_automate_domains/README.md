# Description
Configures, imports, and enables automate domains which are stored in a Git repository.

# UI Depiction
The following shows an image of the adjustments which are being changed, from the UI perspective.

![webmks](./miq_git_automate_ui.png "WebMKS")

# Dependencies
This role has no dependencies.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| enable_no_log                       | no       | boolean       | yes                               | Turn off logging for security reasons (e.g. passwords) |
| miq_automate_git_repos              | yes      | list of dicts | no                                | Ordered list of dics of automate domains which need to be pulled from git (top = lowest priority, bottom = highest priority) |

# Example Variables
The following is an example variable for `miq_automate_git_repos`:

```
miq_automate_git_repos:
  #
  # LOWEST PRIORITY IN AUTOMATION TREE
  #
  - url:        'https://gitlab.com/my_repo/my_miq_code.git'
    verify_ssl: false
    username:   'user'
    password:   'password'
    
  #
  # HIGHEST PRIORITY IN THE AUTOMATION TREE
  - url:        'https://gitlab.com/my_repo/my_miq_variables.git'
    verify_ssl: false
    username:   'user'
    password:   'password'
```
