# Description
This role will fetch the servers current settings from the REST API.

# Dependencies
This role has no external dependencies.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_admin_username                  | yes      | string        | no                                | The admin user used to create the objects via the API |
| miq_admin_password                  | yes      | string        | no                                | The admin user password used to create the objects via the API |
