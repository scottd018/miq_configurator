# Description
This role will change the generically will change advanced settings on the appliance.

# Dependencies
This role depends on the `miq-get_settings` role.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_config_method                   | yes      | string        | yes                               | Integration type to use for configuration: 'rest' or 'rails' are only valid |
| miq_settings_name                   | yes      | string        | no                                | The name (key) of the setting to change |
| miq_settings_value                  | yes      | string        | no                                | The value of the setting to change |
