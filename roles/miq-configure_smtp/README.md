# Description
Configures SMTP.

# UI Depiction
The following shows an image of the adjustments which are being changed, from the UI perspective.

![smtp](./miq_smtp_ui.png "SMTP Settings")

# Dependencies
This role has no dependencies.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_smtp_settings                   | yes      | dict          | no                                | SMTP Settings dict |

# Example Variables
The following is an example variable for `miq_smtp_settings`:

```
miq_smtp_settings:
  authentication:       'none'
  domain:               'scott.net'
  from:                 'svc.cloud@scott.net'
  host:                 'mail.scott.net'
  port:                 25
  username:             'svc.cloud'
  enable_starttls_auto: false
  openssl_verify_mode:  'none'
```
