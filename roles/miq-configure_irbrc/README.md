# Description
Ensures that the $evm object is available for troubleshooting purposes when the rails console is initialized.

# Dependencies
This role has no dependencies.

# Role Variables
This role has no valid variables.

# Example Variables
There should be no need to supply variables here as all variables are defaulted.
