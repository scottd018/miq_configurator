# Description
This role will download common, helpful utilities from Git-based internet sources.

Utilities include:
- Import/Export scripts
- Log parsing scripts
- Log collection scripts

# Dependencies
This role depends on internet connectivity.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
