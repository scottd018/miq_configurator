# Description
This role will configure the settings necessary to load balancer the web server portion of the application.  Alternatively, it will ensure that settings are defaulted if load balancing is not requested.

# Advanced Settings Depiction
Additionally, the advanced settings are updated with this role.  Below is a sample snippet showing the settings that are adjusted:

```
# load balanced
:server:
  :session_store: sql
  
# non load balanced
:server:
  :session_store: cache
```

# Dependencies
This role depends on the `miq-configure_settings` role.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_load_balanced                   | no       | boolean       | yes                               | Configure appliances for load-balancing |
