# Description
This role configures any operating system tuning adjustments that need to occur.

# Dependencies
This role depends on the `miq-configure_elevator` role.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_app_swappiness                  | yes      | integer       | yes                               | The vm.swappiness setting to use |
