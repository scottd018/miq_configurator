# Description
This role will change the timezone in the user interface.

# UI Depiction
The following shows an image of the adjustments which are being changed, from the UI perspective.

![timezone](./miq_app_timezone_ui.png "Timezone")

# Advanced Settings Depiction
Additionally, the advanced settings are updated with this role.  Below is a sample snippet showing the settings that are adjusted:

```
:server:
  :name: miq-appliance
```

# Dependencies
This role depends on the `miq-configure_settings` role.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_app_timezone                    | yes      | string        | yes                               | Timezone to set for the MIQ application |
