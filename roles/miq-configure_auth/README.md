# Description
This role will configuration authentication.  We use External Authentication, which relies upon HTTPD and SSSD at the operating system layer to authenticate the user.  This has proven to be a much more scalable approach to authentication, especially in large environments.

**NOTE**: Only Active Directory authentication is supported at this time.

# UI Depiction
The following shows an image of the adjustments which are being changed, from the UI perspective.

![authentication](./miq_auth_ui.png "Authentication")

# Advanced Settings Depiction
Additionally, the advanced settings are updated with this role.  Below is a sample snippet showing the settings that are adjusted:

```
:authentication:
  :basedn: 
  :bind_dn: 
  :bind_pwd: 
  :bind_timeout: 30
  :follow_referrals: false
  :get_direct_groups: true
  :group_memberships_max_depth: 2
  :ldaphost: 
  :ldapport: '389'
  :mode: httpd
  :search_timeout: 30
  :user_suffix: 
  :user_type: userprincipalname
  :amazon_key: 
  :amazon_secret: 
  :sso_enabled: false
  :saml_enabled: false
  :local_login_disabled: false
  :ldap_role: false
  :httpd_role: true
  :amazon_role: false
```

# Dependencies
This role depends on the `miq-configure_settings` role.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_external_auth_type              | yes      | string        | no                                | Type of external authentication to setup (only active_directory supported right now) |
| miq_ldap_uri                        | yes      | string        | no                                | URI to the LDAP server, to include the port number |
| miq_ldap_base_dn                    | yes      | string        | no                                | Base DN of the LDAP search tree |
| miq_ldap_user_search_base           | yes      | string        | no                                | DN of the user search tree |
| miq_ldap_group_search_base          | yes      | string        | no                                | DN of the group search tree |
| miq_ldap_bind_dn                    | yes      | string        | no                                | DN of the user to bind to LDAP with |
| miq_ldap_bind_password              | yes      | string        | no                                | Password of the user who is binding to LDAP |
| miq_ldap_domain                     | yes      | string        | no                                | LDAP Domain to use |
