# Description
This role will change the timezone of the operating system.

# Dependencies
Timezone should exist via the command `timedatectl list-timezones`.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_os_timezone                     | yes      | string        | yes                               | Timezone to set on the operating system as determined by `timedatectl list-timezones` |
