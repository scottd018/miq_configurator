# Description
This role will add providers to a specific zone.

# UI Depiction
The following shows an image of the adjustments which are being changed, from the UI perspective.

![providers](./miq_providers_ui.png "Providers")

# Dependencies
This role depends on the `manageiq_provider` Ansible module.  The module requires the installation of the manageiq-api-client on the server which is running the Ansible playbook.

Additionally, zones should be created and configured via the `miq-configure_zones` and `miq-configure_zone_assignments` roles.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_providers                       | yes      | list of dicts | no                                | List of dictionary items which represent miq_provider objects from the manageiq_provider module (see https://docs.ansible.com/ansible/latest/modules/manageiq_provider_module.html) |
| miq_zone_name                       | yes      | string        | no                                | Name of the zone to add the providers to |

# Example Variables
The following shows and example of the `miq_providers` variable:

```
miq_providers:
  - name: 'EngVMware2'
    type:  'VMware'
    state: 'present'
    provider:
      hostname: 'vcenter.example.com'
      userid:   'root'
      password: 'password'
```

# Additional Information
Additonal informatio can be found at https://docs.ansible.com/ansible/2.4/manageiq_provider_module.html or https://github.com/ManageIQ/manageiq-api-client-python/.

See also `ansible-doc manageiq_provider` on any host which has Ansible for additional information.  Variables expected to be in the format from that module.
