# Description
This role configures a global region and replication from all child regions.  This is accomplished by using the variables stored on child region appliances, mainly the VMDB target per region, and setting up the replication.

# UI Depiction
The following shows an image of the adjustments which are being changed, from the UI perspective.

![global](./miq_global_ui.png "Global Region")

# Dependencies
This role depends on the `miq-run_rails_code` role.  Additionally, this role should be run after `miq-configure_region_type_remote`.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| enable_no_log                       | no       | boolean       | yes                               | Turn off logging for security reasons (e.g. passwords) |
| miq_vmdb_user                       | yes      | string        | yes                               | The VMDB user that the Global region will login with |
| miq_vmdb_name                       | yes      | string        | yes                               | The VMDB (database) name that the Global region will login into |
| miq_vmdb_port                       | yes      | integer       | yes                               | The port the VMDB is listening on |
| miq_region_number                   | yes      | integer       | no                                | The region number which is being replicated; required to avoid overlap of identical region numbers |
| miq_vmdb_target                     | yes      | string        | no                                | The target database which should be replicated |
