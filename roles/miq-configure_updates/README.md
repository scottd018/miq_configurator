# Description
This role is used to register the system to either the Red Hat portal or to Red Hat Satellite Server for updates.

# Dependencies
This role depends upon `miq-wait_for_application` if the system has been updated and requires a reboot.

# Role Variables (Satellite Server with Activation Key)

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_update_registration_type        | yes      | string        | no                                | Type of registration for updates ('satellite_credentials', 'satellite_activation_key' or 'redhat_portal' are valid). |
| satellite_server                    | yes      | string        | no                                | Satellite Server to register with (valid for 'satellite_credentials' or 'satellite_activaiton_key' miq_update_registration_type). |
| satellite_activation_key            | yes      | string        | no                                | Activation key to use to register to Satellite with (valid for 'satellite_activation_key' miq_update_registration_type). |
| satellite_organization              | yes      | string        | no                                | Satellite organization to register with (valid only for 'satellite_credentials' or 'satellite_activaiton_key' miq_update_registration_type). |

# Role Variables (Satellite Server with Credentials)

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_update_registration_type        | yes      | string        | no                                | Type of registration for updates ('satellite_credentials', 'satellite_activation_key' or 'redhat_portal' are valid). |
| satellite_server                    | yes      | string        | no                                | Satellite Server to register with (valid for 'satellite_credentials' or 'satellite_activaiton_key' miq_update_registration_type). |
| satellite_organization              | yes      | string        | yes                               | Satellite organization to register with (valid only for 'satellite_credentials' or 'satellite_activaiton_key' miq_update_registration_type). |
| satellite_environment               | yes      | string        | yes                               | Satellite lifecycle environment to register into (valid only for 'satellite_credentials' miq_update_registration_type). |
| satellite_username                  | yes      | string        | no                                | Satellite username to register with (valid only for 'satellite_credentials' miq_update_registration_type). | 
| satellite_password                  | yes      | string        | no                                | Satellite password to register with (valid only for 'satellite_credentials' miq_update_registration_type). | 
| satellite_pool_ids                  | no       | list          | yes (auto-attach)                 | GUID of the CloudForms subscription pool IDs to attach to the systems.  Defaults to auto-attach if none specified (valid only for 'satellite_credentials' miq_update_registration_type). |

# Role Variables (Red Hat Portal)

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_update_registration_type        | yes      | string        | no                                | Type of registration for updates ('satellite_credentials', 'satellite_activation_key' or 'redhat_portal' are valid). |
| redhat_portal_username              | yes      | string        | no                                | Red Hat portal username to register with (valid only for 'redhat_portal' miq_update_registration_type). | 
| redhat_portal_password              | yes      | string        | no                                | Red Hat portal password to register with (valid only for 'redhat_portal' miq_update_registration_type). | 
| redhat_portal_pool_ids              | no       | list          | yes (auto-attach)                 | GUID of the CloudForms subscription pool IDs to attach to the systems.  Defaults to auto-attach if none specified (valid only for 'redhat_portal' miq_update_registration_type). |
