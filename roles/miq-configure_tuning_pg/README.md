# Description
This role configures any PostgreSQL tuning adjustments that need to occur.

# Dependencies
This role depends on the `miq-configure_elevator` role.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| *                                   | yes      | various       | yes                               | We are opinionated about the variables here but all are defaulted |
