# Description
This role will enable Capacity & Utilization collection of all hosts, clusters, and datastores which get added to the region.

# UI Depiction
The following shows an image of the adjustments which are being changed, from the UI perspective.

![server_name](./miq_cu_collection_ui.png "Server Name")

# Dependencies
This role has no dependencies.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_cu_collection_clusters_enabled  | yes      | boolean       | yes                               | Enable C&U collection for all hosts and clusters |
| miq_cu_collection_storages_enabled  | yes      | boolean       | yes                               | Enable C&U collection for all datastores |
