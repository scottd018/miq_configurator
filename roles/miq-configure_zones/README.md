# Description
This role creates a zone.

# UI Depiction
The following shows an image of the adjustments which are being changed, from the UI perspective.

![zones](./miq_zones_ui.png "Zoness")

# Dependencies
This role should run before `miq-configure_zone_assignments` which assigns servers to their appropriate zones.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_zone_name                       | yes      | string        | no                                | Name of the zone to create |
| miq_zone_display_name               | yes      | string        | yes                               | Friendly name of the zone to create |
| miq_admin_username                  | yes      | string        | no                                | The admin user used to create the objects via the API |
| miq_admin_password                  | yes      | string        | no                                | The admin user password used to create the objects via the API |
