# Description
This role will change the 'root' user password on the operating system.

# Dependencies
This role has no dependencies, but it should be ran after all other plays run to ensure that Ansible can continue to authenticate for configuration.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_appliance_root_password         | yes      | string        | no                                | Password for the 'root' user |
