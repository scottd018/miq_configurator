# Description
This role will import objects from pre-existing deployments.

The following are examples of what is able of being imported:

- provision_dialogs:                Import provisioning dialogs.
- service_dialogs:                  Import service dialogs.
- service_catalogs:                 Import service catalogs.
- roles:                            Import user roles.
- tags:                             Import tags.
- buttons:                          Import buttons.
- customization_templates:          Import customization templates.
- orchestration_templates:          Import orchestration templates.
- reports:                          Import custom reports.
- widgets:                          Import custom widgets.
- policies:                         Import policies.
- scanitems:                        Import scanitems.
- scriptsrc:                        Import config script sources.
- schedules:                        Import schedules (non-widget type only).

# Dependencies
This role depends on the `miq-download_utilities` role.  Additionally, this role requires a flat directory structure to which desired objects are exported using the `miqexport` feature.

An example Git repository with the previously mentioned flat directory structure:

![git_structure](./miq_import_objects_git.png "Git Directory Structure")

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_objects_repo                    | yes      | dict          | no                                | Repository containing MIQ objects based on the miqimport/miqexport utility |
| miq_objects                         | yes      | list          | no                                | List of objects to import from miq_objects_repo |

# Example Variables
The following is an example variable for `miq_objects_repo` and `miq_objects`:

```
miq_objects_repo:
  repo:     'https://gitlab.com/svc.cloudforms/cfme_supplementary.git'
  dest:     '/tmp/miq_objects_working'
  branch:   'master'
  username: 'svc.cloudforms'
  password: 'P@ssword'
miq_objects:
  - 'provision_dialogs'
  - 'roles'
  - 'service_catalogs'
  - 'service_dialogs'
  - 'tags'
```

# Additional Information
Additional information can be found at https://github.com/rhtconsulting/cfme-rhconsulting-scripts.
