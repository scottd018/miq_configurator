# Description
This role will run a block of rails code.

# Dependencies
This role can only be run on systems which are running the `evmserverd` service.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| enable_no_log                       | no       | boolean       | yes                               | Turn off logging for security reasons (e.g. passwords) |
| title                               | no       | string        | yes                               | The title to print out when running the task.  Helpful because this is executed via several different roles |
| miq_rails_code                      | yes      | block         | no                                | A block of rails code to execute |
