# Description
This roles installs the VMware Virtual Disk Development Kit (VDDK) libraries.  The VDDK SDK is used to run Smart State Analysis against VMware virtual machines.

# Dependencies
This role has no external dependencies.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |

# Additional Information
Additional information can be found at https://code.vmware.com/web/sdk/6.7/vddk and https://access.redhat.com/articles/2078103.
