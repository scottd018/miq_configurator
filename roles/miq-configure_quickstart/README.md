# Description
Configures, imports, and enables automate domains which are stored in a Git repository.

# UI Depiction
The following shows an image of the adjustments which are being changed, from the UI perspective.

![miq_quickstart_domains](./miq_quickstart_automate_domains.png "Miq Quickstart Domains")

# Dependencies
This role depends on miq-import_supplementary_objects and miq-configure_git_automate_domains role.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| enable_no_log                       | no       | boolean       | yes                               | Turn off logging for security reasons (e.g. passwords) |
| miq_quickstart_git_repos            | yes      | list of dicts | yes                               | Required git repos for quickstart enablement, including automate and supplementary objects |
| miq_utilities                       | yes      | dict          | yes                               | Metadata about the necessary miq-utilities repo |
| miq_quickstart                      | yes      | dict          | yes                               | Metadata about the necessary miq-quickstart repo |

# Example Variables
There should be no need to supply variables here as all variables are defaulted.
