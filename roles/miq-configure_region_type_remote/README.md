# Description
This role configures a child region and prepares it for replication to a global region.

# UI Depiction
The following shows an image of the adjustments which are being changed, from the UI perspective.

![remote](./miq_remote_ui.png "Remote Region")

# Dependencies
This role depends on the `miq-run_rails_code` role.  Additionally, this role should be run before `miq-configure_region_type_global`.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_region_type                     | yes      | string        | yes                               | Type of region being deployed (global vs. remote) |
