# Description
Configures the default storage device elevator to an appropriate value for virtualized systems.  If the role detects a change, the system will be rebooted to apply the changes.

# Dependencies
This role has no dependencies.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |

# Additional Information
Additional information can be found at https://access.redhat.com/solutions/5427 or https://access.redhat.com/solutions/109223.