# Description
This role assigns servers to their appropriate zones.

# UI Depiction
The following shows an image of the adjustments which are being changed, from the UI perspective.

![zone_assignments](./miq_zone_assignments_ui.png "Zone Assignments")


# Advanced Settings Depiction
Additionally, the advanced settings are updated with this role.  Below is a sample snippet showing the settings that are adjusted:

```
:server:
  :zone: 'Default Zone'
```

# Dependencies
This role should run after `miq-configure_zones` which creates the zones to ensure of their existence.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_zone_name                       | yes      | string        | no                                | Name of the zone to assign the server to |
