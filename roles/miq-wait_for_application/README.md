# Description
This role is run to check the availability of the application (the `evmserverd` process), to ensure that it is online.

# Dependencies
This role has no external dependencies.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
