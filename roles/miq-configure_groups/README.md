# Description
Creates groups with specified roles.

# UI Depiction
The following shows an image of the adjustments which are being changed, from the UI perspective.

![webmks](./miq_git_automate_ui.png "WebMKS")

# Dependencies
Roles should be pre-existing.  This role should run after `miq-import_supplementary_objects` if roles objects are being imported from a Git repository.  If standard/default roles are being used, then there is no other role dependencies.

# Role Variables

| variable                            | required |  type         | defaulted (see defaults/main.yml) | description |
| ------------------------------------| -------- | ------------- | --------------------------------- | ------------- |
| debug                               | no       | boolean       | yes                               | Enable debug logging |
| miq_groups                          | yes      | list of dicts | no                                | Dictionary of group settings with a role assignment - requires role import from miq_objects/miq_objects_repo |

# Example Variables
The following is an example variable for `miq_groups`:

```
miq_groups:
  - name: 'CloudForms Admins'
    role: 'EvmRole-super_administrator'
```
